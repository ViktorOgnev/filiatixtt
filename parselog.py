#! /usr/local/bin/python3
#
# Note 1:
# In contrast to specification
# task is done in python3,
# as, I think, that would nevertheless be a preferred approach)
#
# Note 2:
# A percentile fn is self-implemented as I really wanted to make it
# no-deps clean. If there were more math stuff to do,
# then i would probably  go for numpy.


import math
import sys


resptimes = []
longest_queries = []

backend_data = {
    'no-response': 0,
}
tmp_data = {}


def linegerator(filepath):
    with open(filepath, 'r+') as fp:
        for line in fp:
            yield line


def check_sndtime(rid, t):
    global longest_queries
    if len(longest_queries) < 10:
        longest_queries.append((rid, t))
    if t > longest_queries[-1][1]:
        longest_queries.pop()
        longest_queries.append((rid, t))
        longest_queries = list(
            reversed(sorted(longest_queries, key=lambda x: x[1])))


def update_errata(data):
    for rg, info_list in data.items():
        backend_data.setdefault(rg, {})
        for entry in info_list:
            backend_data[rg].setdefault(
                entry['url'], {'errors': {}, 'req_count': 0})
            backend_data[rg][entry['url']]['errors'].update(entry['errors'])
            backend_data[rg][entry['url']]['req_count'] += entry['req_count']


def linesumer(ts, rid, etype, opts):
    """
    Consume log line.
    Act according to EventType.

    """

    if etype == 'StartRequest':
        tmp_data[rid] = {}
        tmp_data[rid]['timing'] = {}
        tmp_data[rid]['timing']['req_start'] = ts

    elif etype == 'BackendConnect':
        # opts here would be (REPLICATION_GROUP_NUM, BACKEND_URL)
        replica_info = {'url': opts[1], 'req_count': 0, 'errors': {}}
        if opts[0] not in tmp_data[rid]:
            tmp_data[rid][opts[0]] = [replica_info]
        else:
            tmp_data[rid][opts[0]].append(replica_info)

    elif etype == 'BackendRequest':
        tmp_data[rid].setdefault('responded', {})[opts[0]] = False
        tmp_data[rid][opts[0]][-1]['req_count'] += 1

    elif etype == 'BackendError':
        # opts are (REPLICATION_GROUP_NUM, ERROR_TYPE)
        tmp_data[rid][opts[0]][-1]['errors'].setdefault(opts[1], 0)
        tmp_data[rid][opts[0]][-1]['errors'][opts[1]] += 1

    elif etype == 'BackendOk':
        # opts are (REPLICATION_GROUP_NUM)
        tmp_data[rid]['responded'][opts[0]] = True

    elif etype == 'StartSendResult':
        tmp_data[rid]['timing']['send_start'] = ts

    elif etype == 'FinishRequest':
        timing = tmp_data[rid].pop('timing')
        check_sndtime(rid, ts - timing['send_start'])
        req_time = ts - timing['req_start']
        resptimes.append(req_time)
        # answer to question 4
        backend_data['no-response'] += 1 if not all(tmp_data[rid].pop('responded').values()) else 0
        update_errata(tmp_data[rid])
        # we don't want to store all the log just in another form
        tmp_data.pop(rid)


def percentile(values, percent=0.99):
    """
    Find the percentile of a sorted list of values.
    """
    if not values:
        return None
    k = (len(values) - 1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return values[int(k)]
    d0 = values[int(f)] * (c - k)
    d1 = values[int(c)] * (k - f)
    return d0 + d1


def process_loglines(lines, consume_fn):
    for line in lines:
        timestamp, req_id, evt_type, *options = line.strip().split('\t')
        consume_fn(int(timestamp), req_id, evt_type, options)


def global_info():
    p95 = percentile(sorted(resptimes), 0.95)
    msg = "*" * 100
    msg += (f"\nA 95th percentile is of spend processing requests is {p95} miliseconds\n"
            f"Number of requsts which were not processed in"
            f"time by all replica groups: {backend_data.pop('no-response')}\n"
            f"Here are requests that took most time in a send phase:\n")
    for query in longest_queries:
        msg += f'\t\tRequest id {query[0]} took {query[1]}\n'
    return msg


def show_report():

    gi = global_info()
    print(gi)
    for rgid, nodinfo in sorted(backend_data.items(), key=lambda x: x[0]):
        print(f'In replica group {rgid}')
        for url, d in nodinfo.items():
            print(f"\tOn node {url}")
            print(f"\t\tThere were {d['req_count']} requests")
            print("\t\tAll went fine" if len(
                d['errors']) == 0 else "\t\tThere were the following issues:")
            for t, c in d['errors'].items():
                print(f'\t\t\t{t} occured {c} times')
    print(gi)


def report(lines):
    process_loglines(lines, linesumer)
    show_report()


if __name__ == '__main__':
    try:
        lines = linegerator(sys.argv[1])
    except IndexError:
        print('ERROR: Please provide a logfile path in script call!!')
    else:
        report(lines)
